package com.example.functional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class SimpleExample {
    public static void main(String[] args) {

        ArrayList<Integer> examResultList = new ArrayList<>();
        examResultList.add(50);
        examResultList.add(60);
        examResultList.add(90);
        examResultList.add(30);
        examResultList.add(74);

        System.out.println("Printing all marks : "
                + examResultList);

        System.out.println("after stream operations");

        List<Integer> passed = examResultList.stream()
                .filter(i -> i > 50).collect(Collectors.toList());

        System.out.println(
                "Printing just the passed ones : "
                        + passed);
    }

}
