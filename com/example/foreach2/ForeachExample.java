package com.example.foreach2;

import java.util.Arrays;

class SimpleExample {

        static String findSecondElement(String[] array) {
                int count = 0;
                for (String element : array) {
                        if (count == 1)
                                return element;
                        count++;
                }
                return "";
        }

        public static void main(String[] args) {

                String[] myArray = { "First", "Second", "Third" };
                String secondElement = findSecondElement(myArray);
                System.out.println("without stream: " + secondElement);

                /*
                 * // lambda operations do not allow any external variable operation within
                 * itself.
                 * 
                 * myArray.stream().forEach(s -> {
                 * count++;
                 * System.out.print(s);
                 * });
                 */

                String secondElement2 = Arrays.stream(myArray).limit(2).skip(1).findFirst().orElse("");

                System.out.println("with stream: " + secondElement2);

                // Print all elements
                Arrays.stream(myArray).forEach(element -> System.out.println(element));
        }

}
