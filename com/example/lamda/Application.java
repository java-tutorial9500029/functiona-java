package com.example.lamda;

class Application {
    public static void main(String[] args) {
        
       Analyser analyseObj = new Analyser() {
        @Override
        public int analyse(int x, int y){
            return x*y;
        }
       };
   
   System.out.println(analyseObj.analyse(3,4));
   
    }

}
