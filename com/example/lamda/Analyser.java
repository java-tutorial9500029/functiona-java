package com.example.lamda;

public interface Analyser {

   int analyse(int operand1, int operand2);
}
